var AWS = require('aws-sdk');
AWS.config.region = 'us-east-2';

var s3 = new AWS.S3();
var params = {Bucket: 'web.layer3.pe', Key: 'indice.jpg', Expires: 120};
s3.getSignedUrl('getObject', params, function (err, url) {
  console.log("The URL is", url);
});